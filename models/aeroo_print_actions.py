# -*- coding: utf-8 -*-
#
#    Jamotion GmbH, Your Odoo implementation partner
#    Copyright (C) 2004-2010 Jamotion GmbH.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    Created by gregor.lazzarato on 22.06.2016.
#

from openerp import models, fields, api

import logging

_logger = logging.getLogger(__name__)

class AerooPrintActions(models.TransientModel):
    _inherit = 'aeroo.print_actions'

    additional_notes = fields.Char('Additional notes')

    @api.multi
    def to_print(self):
        context_new = self._context.copy()
        context_new['additional_notes'] = self.additional_notes
        result = super(AerooPrintActions, self.with_context(context_new)).to_print()
        return result
