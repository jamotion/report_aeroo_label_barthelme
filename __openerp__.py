# -*- coding: utf-8 -*-
{
    'name': 'Report Aeroo Product Label Barthelme',
    'version': '8.0.1.1.2',
    'category': '',
    'author':  'Jamotion GmbH',
    'website': 'https://jamotion.ch',
    'summary': 'Subtitle',
    'images': [],
    'depends': [
        'report_aeroo_base',
    ],
    'data': [
         'views/aeroo_print_actions.xml',
         'views/ir_actions_report.xml',
    ],
    'demo': [],
    'test': [],
    'application': False,
}
